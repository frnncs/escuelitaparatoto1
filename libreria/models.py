from django.db import models

class Libro(models.Model):
    nombre = models.CharField(max_length=50)
    autor = models.CharField(max_length=50)
    categoria = models.CharField(max_length=50)
