from django.urls import path
from .views import agregar_libro
from .views import guardar_libro


urlpatterns = [
    path('libro/', agregar_libro, name="libro"),
    path('guardar_libro/', guardar_libro),
]
