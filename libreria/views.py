from django.shortcuts import render
from django.http import JsonResponse
from .models import Libro





def agregar_libro(request):
    template_name = 'libros.html'
    context = {}
    if 'id' in request.GET:
        libro = Libro.objects.get(id=request.GET.get('id'))
        context['libro'] = libro
    
    libros=Libro.objects.all()
    context['libro'] = libros
    return render(request, template_name, context)


def guardar_libro(request):
    context = {'success': 'True'}
    id=request.GET.get('id')
    if id == '':
        libro = Libro(
            autor=request.GET.get('autor'),
            nombre=request.GET.get('nombre'),
            categoria=request.GET.get('categoria')
        )
        libro.save()

    else:
        libro = Libro.objects.get(id=id)
        libro.autor=request.GET.get('autor')
        libro.nombre=request.GET.get('nombre')
        libro.categoria=request.GET.get('categoria')
        libro.save()
    return JsonResponse(context, safe=True)

def retornarID(request):

    context = {}
    idLibro=Libro.objects.getById('#id')
    idLibro.save()
    return JsonResponse(context)
